namespace model;

using System.Drawing.Point;
using com.badlogic.gdx.graphics.OrthographicCamera;
using com.badlogic.gdx.math.Vector2;
using com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
using com.badlogic.gdx.physics.box2d.World;
using com.badlogic.gdx.utils.Timer;
using com.badlogic.gdx.utils.Timer.Task;

/// <summary>
/// An entity subclass that defines and contains the characteristics of a fighter </summary>

public class Fighter : Entity 
{
    private Direction currentDirection { get; set; };
    private Status currentStatus { get; set; };
    private float damage;
    private Timer.Task resetStatusHit;
    private string folderName { get; };
    private string namePlayer { get; };

    /// <summary>
    /// Every time a Fighter is created, a Task that resets the Status to IDLE is also created
    /// and it's called whenever the fighter is hit </summary>
    /// <param name="point"> The Fighter's original point </param>
    /// <param name="width"> The Fighter's width </param>
    /// <param name="height"> The Fighter's height </param>
    /// <param name="type"> The Fighter's BodyType </param>
    /// <param name="world"> The world in which to create the Fighter </param>    /// <param name="initialDirection"> The Fighter's initial direction </param>
    /// <param name="nameFolder"> The Fighter's folder </param>
    /// <param name="namePlayer"> The Fighter's name </param>
    
    public Fighter(Point point, float width, float height, BodyType type, World world,
    			   Direction initialDirection, string folderName, string namePlayer) 
    	: base(new Vector2(point.x, point.y), width, height, type, world)
    {
        this.currentStatus = Status.IDLE;
        this.currentDirection = initialDirection;
        this.Body().UserData = this;
        this.folderName = folderName;
        this.namePlayer = namePlayer;

        ResetStatusHit = new Task() {
            override
            public void Run() {
                this.CurrentStatus = Status.IDLE;
                this.Body().LinearVelocity = (0, 0);
            }
        };
    }

    /// <summary>
    /// The Fighter recognizes that he was attacked, sets his Status to HIT,
    /// updates his damage and calculates his knockback </summary>
    /// <param name="damage"> The damage dealt to the fighter by the incoming attack </param>
    /// <param name="direction"> The direction of the incoming attack </param>
    public void SetHit(float damage, Direction direction) 
    {
        this.damage = this.damage + damage;
        this.damage = 0.3f + this.damage / 100;
        if (this.ResetStatusHit.IsScheduled()) {
            this.ResetStatusHit.Cancel();
        }
        this.currentStatus = Status.HIT;
        Timer.Schedule(ResetStatusHit, this.damage);
        this.Body().LinearVelocity(direction.equals(Direction.RIGHT) ? 25 + damage : - (25 + damage), 60f);
    }

	///<inheritdoc/>
    public override bool IsOut(OrthographicCamera camera) 
    {
        float currentX = this.Body().Position().x - ((camera.viewportWidth * (1 - camera.zoom)) / 2);
        float currentY = this.Body().Position().y - ((camera.viewportHeight * (1 - camera.zoom)) / 2);
        float effectiveWidth = (camera.ViewportWidth * camera.Zoom);
        float effectiveHeight = (camera.ViewportHeight * camera.Zoom);
        if (currentX > 0 && currentX < effectiveWidth && currentY > 0 && currentY < effectiveHeight) {
            return false;
        } else {
            return true;
        }
    }
}
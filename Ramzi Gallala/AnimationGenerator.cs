
using com.badlogic.gdx.Gdx;
using com.badlogic.gdx.graphics.Texture;
using com.badlogic.gdx.graphics.g2d.Animation;
using com.badlogic.gdx.graphics.g2d.TextureRegion;

namespace utility
{

	/// <summary>Used to create an animation starting from the assets folder using the images given</summary>
	class AnimationGenerator
        {

	    public Animation<TextureRegion> loadingBar { get; private set; }
	    private TextureRegion[] loadingBarFrames;
	    private int folderLength;
	    private readonly string animationFolder;
	    private static readonly float ELAPSED_TIME = 1 / 10f;

	    /// <param name="animationFolder"> The folder in which
	    /// the Sprites are contained</param>
	    public AnimationGenerator(readonly string animationFolder)
            {
	        this.animationFolder = animationFolder;
	    	this.folderLength = computeFolderLength();
	        this.loadingBarFrames = new TextureRegion[this.folderLength];
	        this.loadFramesOfLoadingbars();
	        this.loadingBar = new Animation<>(ELAPSED_TIME, loadingBarFrames);
	    }

	    private int ComputeFolderLength()
            {
		int counter = 0;
                while(Gdx.files.internal(this.animationFolder + "/"+ counter +".png").exists())
                {
                     counter++;
	        }
	    return counter;
	    }

	    private void LoadFramesOfLoadingbars()
            {
	        for (int i = 0; i < this.folderLength; i++)
                {
	            this.loadingBarFrames[i] = new TextureRegion(new Texture(this.animationFolder + "/" + i + ".png"));
	        }
	    }

	}
}
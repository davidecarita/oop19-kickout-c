﻿using System;
using System.Collections.Generic;

namespace view.stage
{
    /// <summary>
    /// Draws the Fighters and constantly updates their Sprite
    /// </summary>

    class FighterView
    {

        private InputMultiplexer multiplexer;
        private LinkedList<Pair<Fighter, AnimationManager>> fighters;
        private float elapsedTime {  ;  set; }
        private readonly float rotation;
    
        private const float SCALE = 0.35f;
        private const float VELOCITY_TIME = 1.5f;
        private const float MARGIN_MIN = 25;
        private const float MARGIN_MAX = 60;
        private const float ZOOM_IN_X = 0.01f;
        private const float ZOOM_OUT_X = 0.005f;
        private const float ZOOM_IN_Y = 0.02f;
        private const float ORIGIN_Y = 0f;
        private const float SCALE_Y = 1f;
        private const int DOP = 5;
        private const float WIDTH_FIGHTER = 10;
        private const float HEIGHT_FIGHTER = 15;

        ///<summary>
        /// When FighterView is called, an InputMultiplexer is created 
        /// as well as a LinkedList of all the players currently in the game
        ///  </summary>
        public FighterView()
        {
            this.multiplexer = new InputMultiplexer();
            this.fighters = new LinkedList<Pair<Fighter, AnimationManager>>();
            this.elapsedTime = 0;
            this.rotation = 0;
            //Gdx.input.setInputProcessor(multiplexer);
        }

        /// <summary>
        ///  For each player in the LinkedList, the correct Animation is drawn to the screen
        /// </summary>
        /// <param name="batch"></param>
        public void DrawPlayers(SpriteBatch batch)
        {
            foreach (Pair<Fighter, AnimationManager> player in this.fighters)
            {
                this.elapsedTime = this.elapsedTime + (Gdx.graphics.getDeltaTime() / VELOCITY_TIME);
                batch.draw(player.c2.getAnimation(player.c1.getCurrentStatus()).getKeyFrame(elapsedTime, true),
                           player.c1.getBody().getPosition().x - (WIDTH_FIGHTER / 2),
                           player.c1.getBody().getPosition().y - (HEIGHT_FIGHTER / 2),
                           WIDTH_FIGHTER / 2f, ORIGIN_Y,
                           player.c2.getAnimation(player.c1.getCurrentStatus())
                                        .getKeyFrame(elapsedTime, true).getRegionWidth() * SCALE,
                           player.c2.getAnimation(player.c1.getCurrentStatus())
                                        .getKeyFrame(elapsedTime, true).getRegionHeight() * SCALE,
                           (player.c1.getCurrentDirection().Equals(Direction.RIGHT) ? 1 : -1), SCALE_Y, this.rotation);
            }
        } 

        /// <summary>
        /// Sets the listener
        /// </summary>
        /// <param name="listener"></param>
        public void setListener(InputProcessor listener)
        {
            this.multiplexer.addProcessor(listener);
        }

        /// <summary>
        /// Sets the Fighter's body
        /// </summary>
        /// <param name="body"></param>
        public void setBodyFighter(Fighter body)
        {
            this.fighters.AddLast(new Pair<Fighter, AnimationManager>(body, new AnimationManager(body.getFolderName())));
        }

        /// <summary>
        /// Updates the current view of the game
        /// </summary>
        /// <param name="camera"></param>
        /// <param name="batch"></param>
        public void update(OrthographicCamera camera, SpriteBatch batch)
        {
            float effetivewidth = (camera.viewportWidth * camera.zoom) / 2;
            float effetiveheight = (camera.viewportHeight * camera.zoom) / 2;

            Point position = this.getMaxXY(effetivewidth, effetiveheight, camera);

            if ((effetivewidth - position.x) < MARGIN_MIN)
            {
                camera.zoom = camera.zoom + ZOOM_IN_X;
            }

            if ((effetivewidth - position.x) > MARGIN_MAX)
            {
                camera.zoom = camera.zoom - ZOOM_OUT_X;
            }

            if ((effetiveheight - position.y) < MARGIN_MIN)
            {
                camera.zoom = camera.zoom + ZOOM_IN_Y;
            }
            //camera.zoom = MathUtils.clamp(camera.zoom, 1f, (Gdx.graphics.getDisplayMode().width / DOP) / camera.viewportWidth);

            camera.update();
            batch.setProjectionMatrix(camera.combined);
        }

        private Point getMaxXY(float effetivewidth, float effetiveheight, OrthographicCamera camera)
        {
            HashSet<int> maxW = new HashSet<int>();
            HashSet<int> maxH = new HashSet<int>();
            Point temp = new Point();
            foreach (Pair<Fighter, AnimationManager> player in fighters)
            {
                maxW.Add(this.getPositionCamera(player.c1.getBody().getPosition(), effetivewidth, effetiveheight, camera).x);
                maxH.Add(this.getPositionCamera(player.c1.getBody().getPosition(), effetivewidth, effetiveheight, camera).y);
            }
            temp.setLocation(maxW.stream().max(Integer::compare).get().intValue(),
                             maxH.stream().max(Integer::compare).get().intValue());
            return temp;
        }

        private object getPositionCamera(object p, float effetivewidth, float effetiveheight, OrthographicCamera camera)
        {
            throw new NotImplementedException();
        }

        private Point getPositionCamera(Vector2 position, float width, float height,
                                          OrthographicCamera camera)
        {
            Point temp = new Point();
            temp.x = (int)Math.Abs(position.x - ((camera.viewportWidth * (1 - camera.zoom)) / 2) - width);
            temp.y = (int)Math.Abs(position.y - ((camera.viewportHeight * (1 - camera.zoom)) / 2) - height);
            return temp;
        }

        /// <summary>
        /// Checks if any Fighter is out of the screen
        /// </summary>
        /// <param name="camera"></param>
        /// <returns></returns>
        public Optional Fighter IsAnyoneOut(OrthographicCamera camera)
        {
            foreach (Pair<Fighter, AnimationManager> fighter in fighters)
            {
                if (fighter.c1.isOut(camera))
                {
                    return Optional.ofNullable(fighter.c1);
                }
            }
            return Optional.empty();
        }

    }
}